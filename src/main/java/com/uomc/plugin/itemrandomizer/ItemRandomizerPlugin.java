package com.uomc.plugin.itemrandomizer;

import java.util.Random;
import java.util.stream.IntStream;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemRandomizerPlugin extends JavaPlugin implements Listener {
    // TODO make these config options
    public static final long DELAY = 20L*60L;
    public static final int ITEM_COUNT = 5;
    public static final int DROP_RADIUS = 5;
    private static final double DROP_HEIGHT = 25;

    public Random random;

    public Material getRandomItem() {
        // TODO make curated list of items
        Material[] items = Material.values();
        return items[random.nextInt(items.length)];
    }

    public void dropRandomItem(Location location) {
        ItemStack item = new ItemStack(getRandomItem());
        location.getWorld().dropItemNaturally(location, item);
    }

    public void dropItemsAroundPlayer(Player player) {
        Location playerLocation = player.getLocation();
        IntStream.range(0, ITEM_COUNT).forEach(i -> {
            int xvar = random.nextInt(DROP_RADIUS*2) - DROP_RADIUS;
            int zvar = random.nextInt(DROP_RADIUS*2) - DROP_RADIUS;

            Location dropLocation = new Location(
                    playerLocation.getWorld(),
                    playerLocation.getX() + xvar,
                    playerLocation.getY() + DROP_HEIGHT,
                    playerLocation.getZ() + zvar
                    );

            dropRandomItem(dropLocation);
        });
    }

    public void dropItems() {
        getServer().getOnlinePlayers().forEach(player -> dropItemsAroundPlayer(player));
    }

	@Override
    public void onEnable() {
        random = new Random();

        getLogger().info("enabling plugin " +  this.getName());
        getServer().getPluginManager().registerEvents(this, this);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> dropItems(), DELAY, DELAY);
    }

    @Override
    public void onDisable() {
        getLogger().info("disabling plugin " +  this.getName());
    }

}
